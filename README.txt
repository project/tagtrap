
Tag Trap Module   
---------------

The Tag Trap module allows administrators to specify terms that cannot be submitted 
in a free-tagging taxonomy environment.  This works by checking the terms that have 
been submitted and throwing an error if there are any unacceptable terms given.  This 
will prevent the form from being submitted.

Installation
------------
1.	Follow the regular rules of module installation for Drupal.  Create the folder "modules" 
	at sites/all and then UNZIP this folder into the "modules" directory.
2. 	Enable the module at "Administer", "Site Building", then "Modules".
3. 	You can adjust the disallowed terms on a per vocabulary basis.  Go to "Administer", 
	"Content Management", "Taxonomy", and then click "edit vocabulary" for any vocab you 
	want to disallow terms on.  NOTE: a text field will appear at the end of the form 
	ONLY for vocabs with the "Tags" (free-tagging) option checked.  If the vocab is not 
	free-tagging enabled, you must enabled free-tagging for it to appear.
4.	In the "Exclude terms" field, provide a comma-separated list of words to exclude.  
	Example: zebra,giraffe,leopard,snake
5.	Save the list you've created.
6.	You can try it out by editing or creating a node and adding terms to it that should 
	get caught.
7.	If you disable free-tagging for any vocab, the list will be there when you re-enable 
	free-tagging.
	
Bugs & Known Issues
-------------------
-The validation of submitted node forms is slow because it does a hefty amount of 
preprocessing before it determines if the submitted tags are valid or not.  Caching the 
the preprocessed data doesn't make much sense either, as typically the data is usually 
no more than once.

Maintainers
-----------
-aj045